# Dev Env Setup Script

This script automates the installation of several software dependencies and ensures necessary environment configurations in .bash_profile. It uses Homebrew for most installations.

## Functions
#### command_exists()
	Checks if a command is available in the system’s PATH.
#### add_to_bash_profile_if_needed() 
	Adds export statements to .bash_profile if they are not already present.
#### install_with_brew()
	Install packages using Homebrew.

## Main Script
###	Dependency Installation:	
	Installs the following packages, pausing for user input after each:

	    yarn
	    python
        pip
	    node
	    nvm
	    tfenv
	    terraform
	    awscli
	    postgresql
	    direnv
		svix
##
###	NVM Configuration:
	Configures NVM in .bash_profile if not already configured.
###	Hasura CLI Installation:
	Installs Hasura CLI using a curl command.
###	Playwright Installation:
	Installs Playwright globally using yarn.
###	Completion Message:
	Prompts the user to restart the terminal or source the .bash_profile to apply changes.


