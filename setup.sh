#!/bin/bash

# Function to check if a command is available
command_exists() {
    command -v "$1" &>/dev/null
}

# Function to add export statements to .bash_profile if not already present and necessary
add_to_bash_profile_if_needed() {
    local line="$1"
    local file="$HOME/.bash_profile"
    if ! grep -qF "$line" "$file"; then
        echo "$line" >> "$file"
    fi
}

# Install Homebrew if it's not already installed
if ! command_exists brew; then
    echo "Homebrew not found. Installing Homebrew..."
    /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
    echo "Press [Enter] to continue after Homebrew installation completes..."
    read -r
fi

# Install dependencies with Homebrew
install_with_brew() {
    local package="$1"
    echo "Installing $package with Homebrew..."
    if brew list "$package" &>/dev/null; then
        echo "$package is already installed."
    else
        if brew install "$package"; then
            echo "$package installed successfully."
        else
            echo "Failed to install $package. Moving on."
        fi
    fi
    echo "Press [Enter] to continue..."
    read -r
}

# Install yarn
install_with_brew yarn

# Install Python and pip
install_with_brew python
if ! command_exists pip; then
    echo "pip not found. Installing pip..."
    curl https://bootstrap.pypa.io/get-pip.py -o get-pip.py
    python3 get-pip.py
    rm get-pip.py
    echo "Press [Enter] to continue..."
    read -r
fi

# Install node and nvm
install_with_brew node
install_with_brew nvm

# Configure nvm in .bash_profile if not already configured
if ! grep -q 'export NVM_DIR="$HOME/.nvm"' "$HOME/.bash_profile"; then
    echo "Configuring nvm in .bash_profile..."
    add_to_bash_profile_if_needed 'export NVM_DIR="$HOME/.nvm"'
    add_to_bash_profile_if_needed '[ -s "/usr/local/opt/nvm/nvm.sh" ] && . "/usr/local/opt/nvm/nvm.sh"'
    add_to_bash_profile_if_needed '[ -s "/usr/local/opt/nvm/etc/bash_completion.d/nvm" ] && . "/usr/local/opt/nvm/etc/bash_completion.d/nvm"'
    echo "Press [Enter] to continue..."
    read -r
fi

# Install tfenv and terraform
install_with_brew tfenv
install_with_brew terraform

# Install aws-cli
install_with_brew awscli

# Install PostgreSQL
install_with_brew postgresql

# Install Hasura CLI
echo "Installing Hasura CLI..."
curl -L https://github.com/hasura/graphql-engine/raw/stable/cli/get.sh | bash
echo "Press [Enter] to continue..."
    read -r

# Install Playwright
echo "Installing Playwright..."
yarn global add playwright
echo "Press [Enter] to continue..."
read -r

# Install direnv
install_with_brew direnv

# Install svix
install_with_brew svix/svix/svix

echo "Installation complete. Please restart your terminal or run 'source ~/.bash_profile' to apply changes."
